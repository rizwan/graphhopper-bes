/*
 *  Licensed to GraphHopper and Peter Karich under one or more contributor
 *  license agreements. See the NOTICE file distributed with this work for 
 *  additional information regarding copyright ownership.
 * 
 *  GraphHopper licenses this file to you under the Apache License, 
 *  Version 2.0 (the "License"); you may not use this file except in 
 *  compliance with the License. You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.graphhopper.routing;

import com.graphhopper.routing.util.*;
import com.graphhopper.storage.Graph;
import com.graphhopper.util.Helper;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Peter Karich
 */
public class DijkstraBidirectionRefTest extends AbstractRoutingAlgorithmTester
{
    @Override
    public AlgorithmPreparation prepareGraph( Graph defaultGraph, final FlagEncoder encoder, final Weighting w )
    {
        return new NoOpAlgorithmPreparation()
        {
            @Override
            public RoutingAlgorithm createAlgo()
            {
                return new DijkstraBidirectionRef(_graph, encoder, w);
            }
        }.setGraph(defaultGraph);
    }

    @Test
    public void testCalcAlternatives1()
    {
        Graph graph = createTestGraph();
        AbstractBidirAlgo algo = (AbstractBidirAlgo) prepareGraph(graph).createAlgo();
        List<Path> alternatives = algo.calcAlternatives(0, 7, 3, 0.85, 1.30);
        assertEquals(alternatives.get(0).toString(), 13, alternatives.get(0).getDistance(), 1e-4);
        assertEquals(Helper.createTList(0, 4, 6, 5, 7), alternatives.get(0).calcNodes());
        assertEquals(alternatives.get(1).toString(), 14, alternatives.get(1).getDistance(), 1e-4);
        assertEquals(Helper.createTList(0, 1, 2, 3, 5, 7), alternatives.get(1).calcNodes());
    }

    @Test
    public void testCalcAlternatives2()
    {
        Graph graph = initAlternativeGraph(createGraph(encodingManager, true));
        AbstractBidirAlgo algo = (AbstractBidirAlgo) prepareGraph(graph).createAlgo();
        List<Path> alternatives = algo.calcAlternatives(0, 45, 2, 0.50, 3.00);
        assertEquals(alternatives.get(0).toString(), 9, alternatives.get(0).getDistance(), 1e-4);
        assertEquals(Helper.createTList(0, 1, 11, 12, 22, 23, 33, 34, 44, 45), alternatives.get(0).calcNodes());
        assertEquals(alternatives.get(1).toString(), 18, alternatives.get(1).getDistance(), 1e-4);
        assertEquals(Helper.createTList(0, 1, 2, 3, 4, 5, 15, 25, 24, 34, 44, 45), alternatives.get(1).calcNodes());
    }
    
    @Test
    public void testUpdateBestPathAlternatives(){
        Graph graph = createGraph(encodingManager,true);
        graph.edge(0, 1, 3, true);
        graph.edge(0, 2, 1, true);
        graph.edge(1, 2, 1, true);
        AbstractBidirAlgo algo = (AbstractBidirAlgo) prepareGraph(graph).createAlgo();
        List<Path> alternatives = algo.calcAlternatives(0, 1, 2, 0.50, 3.00);
        assertEquals(alternatives.get(0).toString(), 2, alternatives.get(0).getDistance(), 1e-4);
        assertEquals(Helper.createTList(0, 2, 1), alternatives.get(0).calcNodes());
    }

}
