/*
 *  Licensed to GraphHopper and Peter Karich under one or more contributor
 *  license agreements. See the NOTICE file distributed with this work for 
 *  additional information regarding copyright ownership.
 * 
 *  GraphHopper licenses this file to you under the Apache License, 
 *  Version 2.0 (the "License"); you may not use this file except in 
 *  compliance with the License. You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.graphhopper.routing;

import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.util.Weighting;
import com.graphhopper.storage.EdgeEntry;
import com.graphhopper.storage.Graph;
import com.graphhopper.util.*;
import gnu.trove.iterator.TIntObjectIterator;
import gnu.trove.list.TIntList;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import java.util.*;

/**
 * Calculates best path in bidirectional way.
 * <p/>
 * 'Ref' stands for reference implementation and is using the normal Java-'reference'-way.
 * <p/>
 * @see DijkstraBidirection for an array based but more complicated version
 * @author Peter Karich
 */
public class DijkstraBidirectionRef extends AbstractBidirAlgo
{
    private PriorityQueue<EdgeEntry> openSetFrom;
    private PriorityQueue<EdgeEntry> openSetTo;
    private TIntObjectMap<EdgeEntry> bestWeightMapFrom;
    private TIntObjectMap<EdgeEntry> bestWeightMapTo;
    protected TIntObjectMap<EdgeEntry> bestWeightMapOther;
    protected EdgeEntry currFrom;
    protected EdgeEntry currTo;
    protected PathBidirRef bestPath;
    private boolean updateBestPath = true;

    public DijkstraBidirectionRef( Graph graph, FlagEncoder encoder, Weighting weighting )
    {
        super(graph, encoder, weighting);
        initCollections(1000);
    }

    protected void initCollections( int nodes )
    {
        openSetFrom = new PriorityQueue<EdgeEntry>(nodes / 10);
        bestWeightMapFrom = new TIntObjectHashMap<EdgeEntry>(nodes / 10);

        openSetTo = new PriorityQueue<EdgeEntry>(nodes / 10);
        bestWeightMapTo = new TIntObjectHashMap<EdgeEntry>(nodes / 10);
    }

    @Override
    public void initFrom( int from, double dist )
    {
        currFrom = createEdgeEntry(from, dist);
        bestWeightMapFrom.put(from, currFrom);
        openSetFrom.add(currFrom);
        if (currTo != null)
        {
            bestWeightMapOther = bestWeightMapTo;
            updateBestPath(currTo, from);
        }
    }

    @Override
    public void initTo( int to, double dist )
    {
        currTo = createEdgeEntry(to, dist);
        bestWeightMapTo.put(to, currTo);
        openSetTo.add(currTo);
        if (currFrom != null)
        {
            bestWeightMapOther = bestWeightMapFrom;
            updateBestPath(currFrom, to);
        }
    }

    @Override
    protected Path createAndInitPath()
    {
        bestPath = new PathBidirRef(graph, flagEncoder);
        return bestPath;
    }

    @Override
    protected void initAlternatives( int maxNumOfPlateaux, double maxSimilarityRate, double maxOptimalityRate )
    {
        this.alternatives = new ArrayList<Path>();
        this.maxNumOfPlateaux = maxNumOfPlateaux;
        this.maxSimilarityRate = maxSimilarityRate;
        this.maxOptimalityRate = maxOptimalityRate;
    }

    @Override
    protected void initAlternatives()
    {
        this.alternatives = new ArrayList<Path>();
        this.maxNumOfPlateaux = 3;
        this.maxOptimalityRate = 1.15;
        this.maxSimilarityRate = 0.50;
    }

    @Override
    protected Path extractPath()
    {
        return bestPath.extract();
    }

    @Override
    void checkState( int fromBase, int fromAdj, int toBase, int toAdj )
    {
        if (bestWeightMapFrom.isEmpty() || bestWeightMapTo.isEmpty())
            throw new IllegalStateException("Either 'from'-edge or 'to'-edge is inaccessible. From:" + bestWeightMapFrom + ", to:" + bestWeightMapTo);
    }

    @Override
    public boolean fillEdgesFrom()
    {
        if (openSetFrom.isEmpty())
            return false;

        currFrom = openSetFrom.poll();
        bestWeightMapOther = bestWeightMapTo;
        fillEdges(currFrom, openSetFrom, bestWeightMapFrom, outEdgeExplorer, false);
        visitedCountFrom++;
        return true;
    }

    @Override
    public boolean fillEdgesTo()
    {
        if (openSetTo.isEmpty())
            return false;
        currTo = openSetTo.poll();
        bestWeightMapOther = bestWeightMapFrom;
        fillEdges(currTo, openSetTo, bestWeightMapTo, inEdgeExplorer, true);
        visitedCountTo++;
        return true;
    }

    // http://www.cs.princeton.edu/courses/archive/spr06/cos423/Handouts/EPP%20shortest%20path%20algorithms.pdf
    // a node from overlap may not be on the best path!
    // => when scanning an arc (v, w) in the forward search and w is scanned in the reverseOrder 
    //    search, update extractPath = μ if df (v) + (v, w) + dr (w) < μ            
    @Override
    public boolean finished()
    {
        if (finishedFrom && finishedTo)
            return true;

        return currFrom.weight >= bestPath.getWeight() && currTo.weight >= bestPath.getWeight();
    }

    void fillEdges( EdgeEntry currEdge, PriorityQueue<EdgeEntry> prioQueue,
            TIntObjectMap<EdgeEntry> shortestWeightMap, EdgeExplorer explorer, boolean reverse )
    {
        int currNode = currEdge.adjNode;
        EdgeIterator iter = explorer.setBaseNode(currNode);
        System.out.println(iter);
        while (iter.next())
        {
            if (!accept(iter))
                continue;
            // minor speed up
            if (currEdge.edge == iter.getEdge())
                continue;

            int adjNode = iter.getAdjNode();
            double tmpWeight = weighting.calcWeight(iter, reverse) + currEdge.weight;

            EdgeEntry de = shortestWeightMap.get(adjNode);
            if (de == null)
            {
                de = new EdgeEntry(iter.getEdge(), adjNode, tmpWeight);
                de.parent = currEdge;
                shortestWeightMap.put(adjNode, de);
                prioQueue.add(de);
            } else if (de.weight > tmpWeight)
            {
                EdgeEntry invalidEdge = de;
                prioQueue.remove(invalidEdge);
                de.edge = iter.getEdge();
                de.weight = tmpWeight;
                de.parent = currEdge;
                prioQueue.add(de);
                //if (de != invalidEdge)
                updateInvalidPlateaux(de, invalidEdge);
            }

            if (updateBestPath)
                updateBestPath(de, adjNode);

            addPlateau(de, reverse);
        }
    }

    @Override
    protected void updateBestPath( EdgeEntry shortestEE, int currLoc )
    {
        EdgeEntry entryOther = bestWeightMapOther.get(currLoc);
        if (entryOther == null)
            return;

        // update μ
        double newShortest = shortestEE.weight + entryOther.weight;
        if (newShortest < bestPath.getWeight())
        {
            bestPath.setSwitchToFrom(bestWeightMapFrom == bestWeightMapOther);
            bestPath.setEdgeEntry(shortestEE);
            bestPath.setWeight(newShortest);
            bestPath.setEdgeEntryTo(entryOther);
        }
    }

    protected void addPlateau( EdgeEntry de, boolean reverse )
    {
        //if the edgeEntry node isn't in both the maps we can stop otherwise it will set as plateau start
        if ((de.getPlateauxRef() == -1) && (bestWeightMapOther.get(de.adjNode) != null))
        {
            PathBidirRef p = new PathBidirRef(graph, flagEncoder);

            int newPlateauIndex = alternatives.size() + 1;

            de.plateauxRef = newPlateauIndex;

            // check if adjNode exists in both maps
            EdgeEntry otherMapEdge = bestWeightMapOther.get(de.adjNode);

            if (otherMapEdge != null)
            {
                p.edgeTo = reverse ? de : otherMapEdge;
                p.edgeEntry = reverse ? otherMapEdge : de;

                EdgeEntry tmp = de;

                // while parent = null set the edge as plateau
                while (EdgeIterator.Edge.isValid(tmp.edge))
                {
                    if (tmp.parent.getPlateauxRef() == -1)
                    {
                        otherMapEdge = bestWeightMapOther.get(tmp.parent.adjNode);
                        if (otherMapEdge != null)
                        {
                            otherMapEdge.setPlateauxRef(newPlateauIndex);
                        }
                    } else
                    {
                        break;
                    }
                    tmp = tmp.parent;
                }
                if (optimalityCheck(p, bestPath, maxOptimalityRate))
                    alternatives.add(p);
            }
        }
    }

    private void updateInvalidPlateaux( EdgeEntry de, EdgeEntry invalidEdge )
    {
        for (Path p : alternatives)
        {
            EdgeEntry edge = p.edgeEntry;
            while (edge != null)
            {
                if (edge == invalidEdge)
                {
                    p.edgeEntry = de;
                    break;
                }
                edge = edge.parent;
            }
        }
    }

    @Override
    public String getName()
    {
        return "dijkstrabi";
    }

    TIntObjectMap<EdgeEntry> getBestFromMap()
    {
        return bestWeightMapFrom;
    }

    TIntObjectMap<EdgeEntry> getBestToMap()
    {
        return bestWeightMapTo;
    }

    void setBestOtherMap( TIntObjectMap<EdgeEntry> other )
    {
        bestWeightMapOther = other;
    }

    void setFromDataStructures( DijkstraBidirectionRef dijkstra )
    {
        openSetFrom = dijkstra.openSetFrom;
        bestWeightMapFrom = dijkstra.bestWeightMapFrom;
        finishedFrom = dijkstra.finishedFrom;
        currFrom = dijkstra.currFrom;
        visitedCountFrom = dijkstra.visitedCountFrom;
        // outEdgeExplorer
    }

    void setToDataStructures( DijkstraBidirectionRef dijkstra )
    {
        openSetTo = dijkstra.openSetTo;
        bestWeightMapTo = dijkstra.bestWeightMapTo;
        finishedTo = dijkstra.finishedTo;
        currTo = dijkstra.currTo;
        visitedCountTo = dijkstra.visitedCountTo;
        // inEdgeExplorer
    }

    void setUpdateBestPath( boolean b )
    {
        updateBestPath = b;
    }

    void setBestPath( PathBidirRef bestPath )
    {
        this.bestPath = bestPath;
    }

    @Override
    public List<Path> extractPlateaux( int maxNumOfPlateaux, double maxSimilarityRate, double maxOptimalityRate )
    {
        List<Path> plateaux = new ArrayList<Path>();
        TIntObjectIterator<EdgeEntry> iterator = bestWeightMapFrom.iterator();

        while (iterator.hasNext())
        {
            iterator.advance();
            EdgeEntry currEdge = iterator.value();

            if (currEdge.getPlateauxRef() == -1)
            {
                PathBidirRef p = new PathBidirRef(graph, flagEncoder);

                int newPlateauIndex = alternatives.size() + 1;

                currEdge.setPlateauxRef(newPlateauIndex);

                p.edgeEntry = currEdge;

                while (EdgeIterator.Edge.isValid(currEdge.edge))
                {
                    if (currEdge.parent.getPlateauxRef() == -1)
                    {
                        currEdge.parent.setPlateauxRef(newPlateauIndex);
                    }
                    currEdge = currEdge.parent;
                }

                while (!EdgeIterator.Edge.isValid(currEdge.edge))
                {
                    TIntObjectIterator<EdgeEntry> iterator2 = bestWeightMapTo.iterator();
                    while (iterator2.hasNext())
                    {
                        iterator2.advance();
                        currEdge = iterator2.value();
                        if (currEdge.adjNode == p.edgeEntry.adjNode)
                            p.edgeTo = currEdge;
                    }
                }
                if ((optimalityCheck(p, bestPath, maxOptimalityRate) && (p.distance > 0)))
                {
                    plateaux.add(p);
                }
            }
        }

        plateaux = filterPlateaux(plateaux, maxNumOfPlateaux, maxSimilarityRate);

        return plateaux;
    }

    @Override
    public List<Path> extractPlateaux2()
    {
        Iterator<Path> iterator = alternatives.iterator();
        while (iterator.hasNext())
        {
            Path p = iterator.next();
            p.extract();
            if (p.distance <= 0)
            {
                iterator.remove();
            }
        }
        alternatives = this.filterPlateaux(alternatives, maxNumOfPlateaux, maxSimilarityRate);
        return alternatives;
    }

    private boolean similarityCheck( Path candidatePlateau, Path otherPlateau, double maxSimilarityRate )
    {
        TIntList bestPlateauNodes = otherPlateau.calcNodes();
        TIntList currentPlateauNodes = candidatePlateau.calcNodes();

        if (currentPlateauNodes.containsAll(bestPlateauNodes))
        {
            return false;
        }

        int sizeBeforeIntersection = currentPlateauNodes.size();
        currentPlateauNodes.retainAll(bestPlateauNodes);

        double similarityRate = (double) currentPlateauNodes.size() / sizeBeforeIntersection;

        return similarityRate <= maxSimilarityRate;
    }

    private boolean optimalityCheck( PathBidirRef candidate, PathBidirRef currentBest, double maxOptimalityRate )
    {
        double candidateWeight = candidate.edgeEntry.weight + candidate.edgeTo.weight;
        double currentBestWeight = currentBest.edgeEntry.weight + currentBest.edgeTo.weight;
        double optimalityRate = candidateWeight / currentBestWeight;
        return optimalityRate <= maxOptimalityRate;
    }

    private List<Path> filterPlateaux( List<Path> candidatePlateaux, int maxNumOfPlateaux, double maxSimilarityRate )
    {
        Collections.sort(candidatePlateaux);

        List<Path> plateaux = new ArrayList<Path>();
        plateaux.add(candidatePlateaux.get(0));

        for (int i = 1; i < candidatePlateaux.size(); i++)
        {
            Path candidatePlateau = candidatePlateaux.get(i);
            for (int j = 0; j < plateaux.size(); j++)
            {
                Path otherPlateau = plateaux.get(j);
                {
                    if (similarityCheck(candidatePlateau, otherPlateau, maxSimilarityRate))
                    {
                        plateaux.add(candidatePlateau);
                    }
                }
            }
        }

        if (plateaux.size() > maxNumOfPlateaux)
        {
            plateaux = plateaux.subList(0, maxNumOfPlateaux);
        }
        return plateaux;
    }
}
