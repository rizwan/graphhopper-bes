/*
 * Copyright 2014 dardin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.graphhopper.storage;

import com.graphhopper.routing.*;
import com.graphhopper.routing.util.*;
import com.graphhopper.util.Helper;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author dardin
 */
public class AlternativePathsTest extends GraphHopperStorageTest
{
    @Test
    public void testCalcAlterntives1()
    {
        Graph graph = createGraph();
        initExampleGraph(graph);
        Weighting weighting = new FastestWeighting(carEncoder);
        AbstractBidirAlgo algo = new DijkstraBidirectionRef(graph, carEncoder, weighting);
        List<Path> alternatives = algo.calcAlternatives(0, 1, 3, 0.85, 1.30);
        assertEquals(alternatives.get(0).toString(), 12, alternatives.get(0).getDistance(), 1e-4);
        assertEquals(Helper.createTList(0, 1), alternatives.get(0).calcNodes());
        assertEquals(alternatives.size(), 1);
    }

    @Test
    public void testCalcAlterntives2()
    {
        Graph graph = createGraph();
        initExampleGraph(graph);
        Weighting weighting = new FastestWeighting(carEncoder);
        AbstractBidirAlgo algo = new DijkstraBidirectionRef(graph, carEncoder, weighting);
        List<Path> alternatives = algo.calcAlternatives(0, 2, 3, 0.85, 1.30);
        assertEquals(alternatives.get(0).toString(), 212, alternatives.get(0).getDistance(), 1e-4);
        assertEquals(Helper.createTList(0, 2), alternatives.get(0).calcNodes());
        assertEquals(alternatives.size(), 1);
    }
}
